require "thor"
require "rainbow"
require "rainbow/ext/string"
require "rss"
require "open-uri"
require "fileutils"
require "parallel"
require "version"

Rainbow.enabled = true

module Podioprocess
  is_versioned

  class Cli < Thor

    desc "fetch URL", "fetch the book from the given URL"
    def fetch(url)
      puts " * Source: " + "#{url}".underline
      rss = ""
      targetdir = File.join Dir.getwd, "Audio Books"
      booktitle = ""
      bookauthor = ""
      bookcover = nil

      open (url) do |f|
        rss = f.read
      end

      rssdata = RSS::Parser.parse(rss)
      booktitle = rssdata.channel.title
      bookauthor = rssdata.channel.itunes_author

      unless bookauthor.empty?
        targetdir = File.join targetdir, bookauthor
      end

      unless booktitle.empty?
        targetdir = File.join targetdir, booktitle
      end

      bookcover = rssdata.channel.itunes_image.href

      puts "# Book title: ".bright + booktitle.bright.blue
      puts "## Author: ".bright + bookauthor.bright.blue

      puts " * Output directory: " + targetdir.bright
      FileUtils.mkdir_p targetdir

      unless bookcover.empty? || bookcover.nil?
        filext = bookcover.split('.').last
        coverfile = File.join(targetdir, "cover.#{filext}")

        File.open(coverfile, 'wb') do |fo|
          puts " * Book cover " + bookcover.bright + " saved to " + coverfile.bright + "."
          fo.write open(bookcover).read
        end
      end

      puts "### Book contains " + "#{rssdata.channel.items.size}".bright.blue + " entries."

      Parallel.map(rssdata.channel.items, progress: "Downloading") do |item|
        filename = item.enclosure.url.split("/").last
        targetfile = File.join targetdir, filename

        File.open(targetfile, 'wb') do |fo|
          fo.write open(item.enclosure.url).read
        end
      end


    end

  end

end
