# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'podioprocess'

Gem::Specification.new do |spec|
  spec.name          = "podioprocess"
  spec.version       = Podioprocess::VERSION
  spec.authors       = ["Jan Lindblom"]
  spec.email         = ["janlindblom@fastmail.fm"]
  spec.summary       = %q{Process podiobooks.com RSS feeds.}
  spec.description   = %q{Process podiobooks.com RSS feeds and store the book locally.}
  spec.homepage      = "https://bitbucket.org/janlindblom/podioprocess"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_dependency "rainbow", "~> 2.0.0"
  spec.add_dependency "thor", "~> 0.19.1"
  spec.add_dependency "version", "~> 1.0.0"
  spec.add_dependency "parallel", "~> 1.4.1"
  spec.add_dependency "ruby-progressbar", "~> 1.7.5"
end
