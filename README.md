# Podioprocess

Process RSS feeds from podiobooks.com and save local copies of the files.

Don't expect to much, it's very crude. Does work though, but it doesn't touch
the files downloaded so ID3 tags, cover art etc might need some postprocessing
to make it look good in your player.

Have fun and make sure to check any copyright notices before downloading
anything!

## Installation

Execute:

    $ gem install podioprocess

And then execute:

    $ podioprocess help

## Usage

Get the RSS feed address from podiobooks.com, then use it thusly:

    $ podioprocess fetch URL

## Caveats

There are no tests whatsoever. Runs on Windows (mingw) and Mac OS X as far as
I know.

## Contributing

1. Fork it ( https://bitbucket.org/janlindblom/podioprocess/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
